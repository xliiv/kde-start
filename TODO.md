Remaining Issues

- Pick a good name
Kontrol
KTerminal
KonSplit
KSplonsole
KtermSplit
KSplitsole
They all got the same origin of a name, what if we use something completely different, like:
Klap
Kommand
- crash on window close
- right click actions for split
- decide on better shortcuts
- save window size on quit
- set app icon
- tabs
   - order of tabs is not right when closing them with CTRL-D
   - tab label should auto rename using regular conventions
      - folder, running process, escape function
   - tab rename by double click
   - icons on tabs?

Also
- move to github once we have a good name
- should we create a team account to hold the project?
